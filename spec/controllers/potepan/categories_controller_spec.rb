require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { taxonomy.root }
    let(:child_taxon) { create(:taxon, taxonomy: taxonomy, parent: taxon) }
    let(:product1) { create(:product, taxons: [child_taxon]) }
    let(:product2) { create(:product) }

    before do
      get :show, params: { id: child_taxon.id }
    end

    it "レスポンスの成功" do
      expect(response).to be_success
    end

    it "showテンプレートの表示" do
      expect(response).to render_template(:show)
    end

    it "@taxonomiesの割り当て" do
      expect(assigns(:taxonomies)).to match_array(taxonomy)
    end

    it "@taxonの割り当て" do
      expect(assigns(:taxon)).to eq child_taxon
    end

    it "@productsの割り当て" do
      expect(assigns(:products)).to match_array(product1)
    end

    it "child_taxonに紐づかないproductは非表示" do
      expect(assigns(:products)).not_to include(product2)
    end
  end
end
