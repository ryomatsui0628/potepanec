require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let!(:taxon1) { create(:taxon, name: "bags") }
    let!(:taxon2) { create(:taxon, name: "rails") }
    let!(:product) { create(:product, taxons: [taxon1, taxon2], name: "rails_tote") }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon2]) }

    before do
      get :show, params: { id: product.id }
    end

    it "レスポンスの成功" do
      expect(response).to be_success
    end

    it "showテンプレートの表示" do
      expect(response).to render_template(:show)
    end

    it "@productの割り当て" do
      expect(assigns(:product)).to eq product
    end

    it "関連商品の表示は最大4つまで" do
      expect(assigns(:related_products).size).to eq 4
    end
  end
end
