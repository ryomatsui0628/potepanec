require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "#related_products" do
    let!(:taxon1) { create(:taxon, name: "bags") }
    let!(:taxon2) { create(:taxon, name: "rails") }
    let!(:taxon3) { create(:taxon, name: "apache") }
    let!(:taxon4) { create(:taxon, name: "t-shirts") }
    let!(:product1) { create(:product, taxons: [taxon1, taxon2], name: "rails_tote") }
    let!(:product2) { create(:product, taxons: [taxon1, taxon2], name: "rails_bag") }
    let!(:product3) { create(:product, taxons: [taxon2], name: "rails_mug") }
    let!(:product4) { create(:product, taxons: [taxon3, taxon4], name: "apache_jersey") }

    it "適切な関連商品の取得" do
      expect(Spree::Product.related_products(product1)).to match_array([product2, product3])
    end

    it "関連しない商品は取得しない" do
      expect(Spree::Product.related_products(product1)).not_to include(product4)
    end

    it "商品自身は関連商品として取得しない" do
      expect(Spree::Product.related_products(product1)).not_to include(product1)
    end
  end
end
