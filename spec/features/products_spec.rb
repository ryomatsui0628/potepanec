require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given!(:taxon1) { create(:taxon, name: "bags") }
  given!(:taxon2) { create(:taxon, name: "rails") }
  given!(:product) { create(:product, taxons: [taxon1, taxon2], name: "rails_tote", price: 15.99) }
  given!(:unrelated_product) { create(:product, name: "apache_jersey", price: 19.99) }
  given!(:related_product) { create(:product, taxons: [taxon2], name: "rails_mug", price: 13.99) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario "商品および関連商品の適切な表示" do
    expect(page).to have_title product.name
    within '.singleProduct' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
    within '.productCaption' do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
      expect(page).not_to have_content product.name
      expect(page).not_to have_content product.display_price
      expect(page).not_to have_content unrelated_product.name
      expect(page).not_to have_content unrelated_product.display_price
    end
  end

  scenario "関連商品のリンクから適切な商品詳細ページに遷移する" do
    click_on related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
    within '.singleProduct' do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
      expect(page).not_to have_content product.name
      expect(page).not_to have_content product.display_price
      expect(page).not_to have_content unrelated_product.name
      expect(page).not_to have_content unrelated_product.display_price
    end
    within '.productCaption' do
      expect(page).not_to have_content related_product.name
      expect(page).not_to have_content related_product.display_price
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).not_to have_content unrelated_product.name
      expect(page).not_to have_content unrelated_product.display_price
    end
  end
end
