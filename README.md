# ポテパンキャンプECサイトリポジトリ
Solidusを使用したECサイトを通じて、ポテパンキャンプの4つの課題に取り組んだものです。

## 主な取り組み
・開発環境構築

・商品表示に関する実装(商品詳細ページ、カテゴリ一覧表示、関連商品表示)

・Rspec(controller spec, model spec, feature spec)

![](docs/images/installation/first_view.png)

以下は課題の内容と実装したページへのリンクです。

## 課題1
・開発環境のセットアップ

https://cryptic-retreat-92273.herokuapp.com/potepan/index.html

## 課題2
・商品詳細ページのテンプレート（potepan/sample/single_product.html.erb）を参考にして、商品詳細ページを
　実装してください。
・商品のモデル名は Spree::Product です。
・実装するパス は /potepan/products としてください。
・ルーティングの定義には適切に namespace を利用してください。
・app/views/layouts/application.html.erb を使用して、ヘッダーの共通化を行ってください。

https://cryptic-retreat-92273.herokuapp.com/potepan/products/1

## 課題3
・カテゴリーページのテンプレート（potepan/sample/product_grid_left_sidebar.html.erb）に選択したカテゴリ　ーの商品が一覧で表示されるようにしてください。
・カテゴリー引数は taxonomies の id を取るように実装してください。
・実際のパスは /potepan/categories/:taxon_id/ となります。

https://cryptic-retreat-92273.herokuapp.com/potepan/categories/1

## 課題4
・課題２で作成した商品ページのページ下部に表示している商品と同じカテゴリーに属する商品が表示されるように
　実装してください。

https://cryptic-retreat-92273.herokuapp.com/potepan/products/1
